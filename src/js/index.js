import '../../node_modules/bootstrap/scss/bootstrap.scss';
import '../scss/style.scss';
import { Model } from "./model.js";
import { View } from "./view.js";

let modelInstance = new Model();
let viewInstance = new View (modelInstance);

viewInstance.initialization();