/**
 * La classe Todo représente un élément Todo sous forme de données.
 */
export class Todo {
    constructor(paramLabel, paramPriority, paramChecked = false) {
        this.label = paramLabel;
        this.priority = paramPriority;
        this.checked = paramChecked;
    }
    /**
     * Méthode qui cochera ou décochera un Todo selon son état
     * actuel
     */
    toggle() {
        this.checked = !this.checked;
        /*
        if(this.checked) {
            this.checked = false;
        }else {
            this.checked = true;
        }
        */
        
    }
}