import { Todo } from "./todo.js";

/**
 * La classe Model représente notre todo list sous forme de données
 * pures, complètement indépendante de l'affichage. Le Model ne doit
 * contenir aucune référence au HTML, au DOM ou quoique ce soit de
 * visuel.
 * On s'en sert ici pour stocker nos Todo dans une liste, mais aussi
 * pour faire des méthodes qui ajouteront ou supprimeront des Todo
 * de la liste en question
 */
export class Model {
    constructor() {
        this.list = [];
        this._load();
    }
    /**
     * Une méthode qui ajoutera un nouveau todo à la liste de l'instance
     * de la classe Model
     * @param {Todo} nouveauTodo le nouveau Todo à ajouter à la liste
     */
    addTodo(nouveauTodo) {
        //On vérifie si la valeur de nouveauTodo est
        //bien une instance de la classe Todo avec instanceof
        if (nouveauTodo instanceof Todo) {
            this.list.push(nouveauTodo);
            this._save();
        }
    }
    /**
     * Une méthode qui supprimera un Todo de la liste de l'instance
     * de la classe Model
     */
    removeTodo() {
        /*
        Pour chaques éléments de la liste, on verifie que les todos sont cochés.
        Si c'est le cas, on le supprime.
        */
        for (let index = this.list.length - 1; index >= 0; index--) {
            if (this.list[index].checked) {
                this.list.splice(index, 1);
            }
        }
        this._save();

        /*
        //Deux autres façons de faire la même chose
        this.list = this.list.filter(function(item) {
            return !item.checked;
        });
        //ici en utilisant une fat arrow function (on en parle bientôt)
        this.list = this.list.filter(item => !item.checked);
        */
    }

    /**
     * Une méthode "privée" qui sauvegarde les todos dans le localStorage.
     */
    _save(){
        let json = JSON.stringify(this.list);
        localStorage.setItem('list', json);
    }

    /**
     * Une méthode "privée" qui récupère les todos dans le localStorage.
     */
    _load(){
        let json = localStorage.getItem('list');
        let obj;

        JSON.parse(json) ? obj = JSON.parse(json) : obj = [];
        /*
        équivalent en if/else de l'opérateur ternaire :
        if (JSON.parse(json)) {
            obj = JSON.parse(json);
        } else {
            obj = [];
        }
        */

        /*
        Pour chaque éléments du localStorage, on recréé l'instance des todos et la pousse dans le tableau.
        */
        for (let index = 0; index < obj.length; index++) {
            let currentTodo = obj[index];
            let todoInstance = new Todo (currentTodo.label, currentTodo.priority);
            this.list.push(todoInstance);
        }
    }
}